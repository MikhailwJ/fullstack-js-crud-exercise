export interface Employee {
  id: number;
  name: string;
  code: string;
  profession: string;
  color: string;
  city: string;
  branch: string;
  assigned: boolean;
}

export interface AppState {
  employees: Employee[];
}

export interface EmployeeFormFields {
  name: string;
  value: any;
  placeholder: string;
  type: string;
  required: boolean;
}

export interface EmployeeForm {
  id?: number;
  fields: EmployeeFormFields[];
}

export interface EmployeeListProp {
  data: Employee[];
  updateOrCreate: (employee: Employee) => void;
  remove: (id: number) => void;
}

export interface EmployeeListState {
  form: EmployeeForm;
  modalIsOpen: boolean;
  modalType: string;
  mode: number;
}
