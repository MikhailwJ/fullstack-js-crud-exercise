import Axios from 'axios';
import React from 'react';
import { AppState, Employee } from './types/employee';
import { Header } from './components/Header';
import { Container, Box } from '@material-ui/core';
import EmployeeList from './components/EmployeeList';

export class App extends React.Component<any, AppState> {
  componentWillMount() {
    this.setState({ employees: [] });
  }

  componentDidMount() {
    Axios.get<Employee[]>('http://localhost:8080/api/employees')
      .then(response => response.data)
      .then(employees => this.setState({ employees }));
  }

  updateOrCreateEmployee = (employee: Employee) => {
    const employees = this.state.employees.slice();
    const index = employees.findIndex(e => e.id === employee.id);
    if (index === -1) {
      employees.push(employee);
    } else {
      employees[index] = { ...employees[index], ...employee };
    }
    this.setState({ employees });
  };

  removeEmployee = (id: number) => this.setState({ employees: this.state.employees.filter(e => e.id !== id) });

  render() {
    const { employees } = this.state;

    console.log(this.state);

    return (
      <div className='App'>
        <Header title='Plexxis Employees' />
        <Container>
          <Box my={4}>
            <EmployeeList data={employees} updateOrCreate={this.updateOrCreateEmployee} remove={this.removeEmployee} />
          </Box>
        </Container>
      </div>
    );
  }
}
