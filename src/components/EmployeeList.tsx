import React, { Component } from 'react';
import { Employee, EmployeeListProp, EmployeeListState } from '../types/employee';
import Axios, { AxiosResponse } from 'axios';
import { Box, Button, Fab, Dialog, DialogTitle, DialogContent, TextField, DialogActions, MenuItem } from '@material-ui/core';
import ReactTable from 'react-table';
import { startCase } from 'lodash';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

export default class EmployeeList extends Component<EmployeeListProp, EmployeeListState> {
  initForm = () => ({
    id: undefined,
    fields: [
      { name: 'name', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'code', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'profession', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'color', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'city', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'branch', value: undefined, placeholder: '', type: 'text', required: false },
      { name: 'assigned', value: false, placeholder: '', type: 'boolean', required: false }
    ]
  });
  private iniState = { modalIsOpen: false, modalType: '', form: this.initForm() };

  componentWillMount() {
    this.setState(this.iniState);
  }

  openModal(mode = 0) {
    let modalType;
    switch (mode) {
      case 1:
        modalType = 'Edit';
        break;
      case 2:
        modalType = 'Delete';
        break;
      default:
        modalType = 'Create';
        break;
    }
    this.setState({ modalIsOpen: true, modalType, mode });
  }

  closeModal = () => {
    this.setState({ modalIsOpen: false, modalType: '', form: this.initForm() }, () => console.log(this.iniState, this.state));
  };

  onCreate = () => this.openModal();

  onEdit = (data: Employee) => {
    const fields = this.initForm().fields.map(prop => ({
      ...prop,
      value: prop.type === 'boolean' ? !Boolean(data[prop.name] === 'false' || data[prop.name] === false || data[prop.name] === 0) : data[prop.name]
    }));
    this.setState({ form: { id: data.id, fields } }, () => this.openModal(1));
  };

  onDelete = (data: Employee) => this.setState({ form: { id: data.id, fields: [] } }, () => this.openModal(2));

  handleChange = e => {
    const form = { ...this.state.form };
    const items = form.fields.slice();
    const item = items.find(prop => prop.name === e.target.name);
    if (item) item.value = e.target.value;
    this.setState({ form: { ...form, fields: [...items] } });
    console.log(this.state);
  };

  onSubmit = async e => {
    try {
      e.preventDefault();
      const { form } = this.state;
      const url = 'http://localhost:8080/api/employees';
      const userUrl = `${url}/${form.id}`;
      const submit = form.fields.reduce(
        (props, prop) => ({
          ...props,
          [prop.name]: prop.type === 'boolean' ? !Boolean(prop.value === 'false' || prop.value === false || prop.value === 0) : prop.value
        }),
        {}
      );
      let res: AxiosResponse;
      switch (this.state.mode) {
        case 1:
          res = await Axios.put<Employee>(userUrl, submit);
          this.props.updateOrCreate(res.data);
          break;
        case 2:
          res = await Axios.delete(userUrl);
          form.id && this.props.remove(Number(form.id));
          break;
        default:
          res = await Axios.post<Employee>(url, submit);
          this.props.updateOrCreate(res.data);
          break;
      }
    } catch (error) {
      console.log(error.message);
    }
    this.closeModal();
  };

  render() {
    const { data } = this.props;
    return (
      <div>
        <Box display='flex' my={2} justifyContent='center'>
          <Button variant='outlined' onClick={this.onCreate}>
            Create
          </Button>
        </Box>
        <ReactTable
          data={data}
          columns={[
            { Header: 'id', accessor: 'id' },
            { Header: 'name', accessor: 'name' },
            { Header: 'code', accessor: 'code' },
            { Header: 'profession', accessor: 'profession' },
            { Header: 'color', accessor: 'color' },
            { Header: 'city', accessor: 'city' },
            { Header: 'branch', accessor: 'branch' },
            { Header: 'assigned', accessor: 'assigned', Cell: row => `${Boolean(row.value)}` },
            {
              Header: 'Action',
              Cell: row => (
                <div>
                  <Fab color='secondary' style={{ margin: '0 5px' }} size='small' aria-label='Edit' onClick={() => this.onEdit(row.original)}>
                    <EditIcon />
                  </Fab>
                  <Fab style={{ margin: '0 5px' }} size='small' aria-label='Delete' onClick={() => this.onDelete(row.original)}>
                    <DeleteIcon />
                  </Fab>
                </div>
              )
            }
          ]}
        />
        <Dialog open={this.state.modalIsOpen} onClose={this.closeModal} aria-labelledby='form-dialog-title'>
          <DialogTitle id='form-dialog-title'>{`${this.state.modalType} Employee: ${this.state.form.id || ''}`}</DialogTitle>
          <DialogContent>
            <form onSubmit={this.onSubmit} id='employee-form'>
              {this.state.form.fields.map((item, i) => (
                <TextField
                  key={i}
                  name={item.name}
                  label={startCase(item.name)}
                  value={item.type === 'boolean' ? Boolean(item.value) : undefined}
                  defaultValue={item.type !== 'boolean' ? item.value : undefined}
                  onChange={this.handleChange}
                  margin='normal'
                  fullWidth={true}
                  select={item.type === 'boolean'}
                >
                  {item.type === 'boolean'
                    ? ['true', 'false'].map(value => (
                        <MenuItem key={value} value={value}>
                          {value}
                        </MenuItem>
                      ))
                    : undefined}
                </TextField>
              ))}
            </form>
          </DialogContent>
          <DialogActions>
            <Button type='submit' color='primary' variant='contained' form='employee-form'>
              submit
            </Button>
            <Button onClick={this.closeModal} variant='contained'>
              cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
