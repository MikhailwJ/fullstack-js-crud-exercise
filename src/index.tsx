import React from "react";
import ReactDOM from "react-dom";
import "react-table/react-table.css";
import { App } from "./App";
import * as serviceWorker from "./registerServiceWorker";
import { ThemeProvider } from "@material-ui/styles";
import { CssBaseline, createMuiTheme } from "@material-ui/core";
import { red } from "@material-ui/core/colors";

const baseTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#556cd6"
    },
    secondary: {
      main: "#19857b"
    },
    error: {
      main: red.A400
    },
    background: {
      default: "#fff"
    }
  }
});

ReactDOM.render(
  <ThemeProvider theme={baseTheme}>
    <CssBaseline />
    <App />
  </ThemeProvider>,
  document.getElementById("root")
);

serviceWorker.register();
