require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
const employees = require('./data/employees.json');
const { Knex } = require('./knexfile');
const table = 'employees';
let useDB = false;

const corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
};

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors(corsOptions));

function findByIndexId(id) {
  const index = employees.findIndex(e => e.id === +id);
  if (index === -1) throw new Error('Not found');
  return index;
}

function handelError(res, error) {
  return res.status(404).json({ error: error.message });
}

app.get('/api/employees', async (req, res, next) => {
  try {
    console.log('[GET] /api/employees');
    res.setHeader('Content-Type', 'application/json');
    res.status(200);
    res.json(useDB ? (await Knex(table)) || [] : employees);
  } catch (error) {
    handelError(res, error);
  }
});

app.post('/api/employees', async (req, res, next) => {
  try {
    console.log('[POST] /api/employees');
    let employee;
    if (useDB) {
      const id = await Knex(table).insert(req.body);
      employee = await Knex(table)
        .where({ id })
        .first();
    } else {
      employee = { id: employees.length + 1, ...req.body };
      employees.push(employee);
    }
    res.status(201);
    res.json(employee);
  } catch (error) {
    handelError(res, error);
  }
});

app.put('/api/employees/:id', async (req, res, next) => {
  try {
    console.log('[PUT] /api/employees');
    const id = req.params.id;
    let employee;
    if (useDB) {
      await Knex(table)
        .where({ id })
        .update(req.body);
      employee = await Knex(table)
        .where({ id })
        .first();
    } else {
      const index = findByIndexId(id);
      employee = employees[index] = { ...employees[index], ...req.body };
    }
    res.status(201);
    res.json(employee);
  } catch (error) {
    handelError(res, error);
  }
});

app.delete('/api/employees/:id', async (req, res, next) => {
  try {
    console.log('[DELETE] /api/employees');
    const id = req.params.id;
    if (useDB) {
      await Knex(table)
        .where({ id })
        .del();
    } else {
      const curentEmployees = employees.filter(e => e.id !== +id);
      employees.length = 0;
      employees.push(...curentEmployees);
    }
    res.status(204);
    res.json();
  } catch (error) {
    handelError(res, error);
  }
});

Knex(table)
  .first()
  .then(() => {
    console.log('using DB');
    useDB = true;
  })
  .catch(error => {
    console.log(error.message);
    console.log('defauling to file provided...');
    useDB = false;
  });

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'));
