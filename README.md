# Plexxis Interview Exercise

## This app will us MySQL as a persistant store if credentials are provided via a .env, otherwise it will just use the data file that was provided.

## Requirements

- NodeJs
- Mysql (optional)

## Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). The front-end app runs off localhost:3000. The REST API is located in the /server folder and runs off localhost:8080. The data is being served from a JSON file located in the /server/data folder.

1. yarn install or npm install
2. If you want to test with MySQL manually run the attached migration file located at server\migration.sql
3. Run `npm start` to start both servers.

## What i did

- I used Material UI to satisfy the (looks good, works well, has intuitive design, etc.) condition.
- Converted react from js files to .tsx | .ts
- Borrowed logic from an old app i did to save on time
- Didn't bother doing much with the server other than what was reqiured.

## Would have done (TODO?)

- Implement server side validation for requests
- Seperate frontend from the backend into their own repos
- Add a dockerfile
